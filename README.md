The Morris water maze is a widely used behavioral test for rodents. It can test learning, memory and spatial
working with great accuracy. The performance of rodents in Morris water maze can reflect the deficit in the
dentate gyrus.

The maze consisted of a 122-cm-diameter pool filled with the water. In spatial learning tests, mice were
first tested their performance with a visible platform to exclude the possibility that the mice had vision
impairment. Mice were allowed to swim freely for 60 s, and sit on the platform for 20s. And if the mice were
not able to find the visible platform within 60 s, the mice were guided to the platform by the experimenter
and was allowed to sit on the platform for an extra 10 s.

For the hidden platform, the platform was submerged 0.8 cm below the surface of the water. The platform
location remained the same but the starting points varied randomly among the four daily trails. The interval
between two trails was 15 minutes. If the mice couldnít found the platform in 60s, the mice were guided to
the platform by the experimenter.

After the five-day learning phase, the platform was removed to test mice memory retention of platform location.
The probe trial were performed 24 h after the last learning trail. The mice were allowed to swim freely for 60s,
and the entrance numbers of the platform were recorded by a video camera tracking system.

In this study, the mouse line I used for test is F39, from crossing 5xFAD and N40K (two neurondegenerativer mouse
model). There are four groups in my study: double transgenic mice (FAD+N40K), FAD, N40K, WT. The goal of my 
experiment is to explore if the performance of double transgenic mouse is worse than 5xFAD and N40K.

